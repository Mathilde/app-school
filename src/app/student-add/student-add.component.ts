import {Component, Input, OnInit} from '@angular/core';
import {StudentInfosService} from "../services/student-infos.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {RawStudentInfos} from "../models/student-raw-infos";
import {Speciality} from "../models/speciality.enum";

@Component({
  selector: 'app-student-add',
  templateUrl: './student-add.component.html',
  styleUrls: ['./student-add.component.css']
})
export class StudentAddComponent implements OnInit {

  @Input()
  selectedSpeciality: Speciality;

  speciality = Speciality;
  studentForm: FormGroup;

  constructor(private fb: FormBuilder , private studentService : StudentInfosService, private router : Router) {
    this.studentForm = this.fb.group({
      pseudo : ['', Validators.required ],
      lastname : ['', Validators.required ],
      firstname : ['', Validators.required ],
      birthdate : ['', Validators.required ],
      speciality : ['', Validators.required],
      email : ['', Validators.required ],
      phone : ['', Validators.required ],
    });
  }

  createStudent(){
    const formModel = this.studentForm.value;
    const rawStudent : RawStudentInfos = {
      pseudo : formModel.pseudo,
      lastname : formModel.lastname,
      firstname : formModel.firstname,
      birthdate : formModel.birthdate,
      speciality : this.selectedSpeciality,
      email : formModel.email,
      phone : formModel.phone,
    }
    this.studentService.add(rawStudent).subscribe((a)=>
      this.router.navigate(['/admin/students'])
    );
  }

  getSpecialities() {
    return Object.values(Speciality);
  }
  ngOnInit() {
  }

}
