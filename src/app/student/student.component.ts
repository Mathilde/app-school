import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentInfosService} from "../services/student-infos.service";
import {StudentInfos} from "../models/student-infos";
import {StudentGrade} from "../models/student-grade";
import {StudentGradeService} from "../services/student-grade.service";

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  pseudo : string
  studentInfos : StudentInfos
  private _studentGrades : StudentGrade[]
  moyenne : number

  constructor(private route: ActivatedRoute, private router : Router, private studentInfosService: StudentInfosService,
              private studentGradeService : StudentGradeService) {
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      if (params && params['pseudo']) {
        this.pseudo = params['pseudo'];
      }
    });
    this.getStudentInfosByPseudo(this.pseudo);
  }

  public getStudentInfosByPseudo(pseudoValue: string) {
    // this.studentInfosService.getAll().subscribe((students) => {
    //   this.studentInfos = students.filter(student => student.pseudo == pseudoValue)[0];
    //   if(this.studentInfos) {
    //     this.getGradesList(this.studentInfos.id);
    //   } else {
    //     this.router.navigate(['/home'], { queryParams: { pseudo: this.pseudo } });
    //   }
    // });

    this.studentInfosService.getByPseudo(pseudoValue).subscribe((students) => {
        this.studentInfos = students[0];
        if(this.studentInfos) {
          this.getGradesList(this.studentInfos.id);
        } else {
          this.router.navigate(['/home'], { queryParams: { pseudo: this.pseudo } });
        }
    })
  }

  public getGradesList(idValue : number) {
    this.studentGradeService.getByStudentId(idValue).subscribe((grades) => {
      this._studentGrades = grades;
      this.calculMoyenne();
    })

    // this.studentGradeService.getAll().subscribe((grades) => {
    //   this._studentGrades = grades.filter(grade => grade.student_id == idValue);
    //   this.calculMoyenne();
    // });

  }

  public studentGrades() {
    return this._studentGrades;
  }

  public calculMoyenne() {
    let diviseur = 0;
    let dividende = 0;
    this._studentGrades.forEach(item => {
      diviseur += item.coefficient;
      dividende += (item.note * item.coefficient);
    });
    this.moyenne = dividende / diviseur;
  }

  public getMoyenne() {
    return this.moyenne;
  }

  deleteStudent(student : StudentInfos){
    this.studentInfosService.delete(student.id).subscribe(() =>
      this.router.navigate(['/admin/students']))
  }

  public getStudent() {
    return this.studentInfos;
  }

  public isAdmin() {
    return this.router.url === '/admin/student/'+this.studentInfos.pseudo;
  }

  public isUtilisateur() {
    return this.router.url === '/student/'+this.studentInfos.pseudo;
  }
}
