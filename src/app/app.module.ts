import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import { environment } from '../environments/environment';
import { HomeComponent } from './home/home.component';
import { StudentComponent } from './student/student.component';
import { StudentInfosService } from "./services/student-infos.service";
import { StudentInfosComponent } from './student-infos/student-infos.component';
import { StudentGradesComponent } from './student-grades/student-grades.component';
import { StudentGradeService } from "./services/student-grade.service";
import {CommonModule} from "@angular/common";
import { AdminComponent } from './admin/admin.component';
import { StudentAddComponent } from './student-add/student-add.component';
import { StudentUpdateComponent } from './student-update/student-update.component';
import { AddNoteComponent } from './add-note/add-note.component';
import { UpdateNoteComponent } from './update-note/update-note.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'student/:pseudo', component: StudentComponent },
  { path: 'admin/students', component: AdminComponent },
  { path: 'admin/add', component: StudentAddComponent },
  { path: 'admin/student/:pseudo', component: StudentComponent },
  { path: 'admin/student/:id', component: StudentComponent }, //
  { path: 'admin/update/:id', component: StudentUpdateComponent },
  { path: 'admin/addgrade/:id', component: AddNoteComponent},
  { path: 'student/update/:id', component: StudentUpdateComponent },
  { path: 'admin/updategrade/:id', component: UpdateNoteComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StudentComponent,
    StudentInfosComponent,
    StudentGradesComponent,
    AdminComponent,
    StudentAddComponent,
    StudentUpdateComponent,
    AddNoteComponent,
    UpdateNoteComponent
  ],
  imports: [
    RouterModule.forRoot(
    appRoutes,
    { enableTracing: false } // <-- debugging purposes only
  ),
    BrowserModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    
  ],
  providers: [StudentInfosService, StudentGradeService],
  bootstrap: [AppComponent]
})

export class AppModule { }
