import {Component, Input, OnInit} from '@angular/core';
import {StudentGrade} from "../models/student-grade";
import {StudentGradeService} from "../services/student-grade.service";
import {ActivatedRoute, Router} from "@angular/router";
import {StudentInfos} from "../models/student-infos";

@Component({
  selector: 'app-student-grades',
  templateUrl: './student-grades.component.html',
  styleUrls: ['./student-grades.component.css']
})
export class StudentGradesComponent implements OnInit {

  @Input()
  studentGrades:  StudentGrade[];

  @Input()
  moyenne : number;

  @Input()
  studentInfos : StudentInfos;

  columns : String[]

  constructor(private route: ActivatedRoute, private router : Router, private studentGradesService: StudentGradeService) {
  }

  ngOnInit() {
    this.columns = this.studentGradesService.getColumns();
  }

  isAdmin() {
    return this.router.url.indexOf('admin') !== -1;
  }

  deleteGrade(grade: StudentGrade){
    this.studentGradesService.delete(grade.id).subscribe(() => this.refreshGrades());
  }

  refreshGrades() {
    this.studentGradesService.getByStudentId(this.studentInfos.id).subscribe((grades) => {
      this.studentGrades = grades;
      this.calculMoyenne();
    })
  }

  public calculMoyenne() {
    let diviseur = 0;
    let dividende = 0;
    this.studentGrades.forEach(item => {
      diviseur += item.coefficient;
      dividende += (item.note * item.coefficient);
    });
    this.moyenne = dividende / diviseur;
  }

  public getColumnName(col : string) {
    return col.substr(0,1).toUpperCase() + col.substr(1,col.length);
  }
}
