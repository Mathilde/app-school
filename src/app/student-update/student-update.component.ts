import { Component, OnInit, Input } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {StudentInfos} from "../models/student-infos";
import {ActivatedRoute, Router} from '@angular/router';
import {StudentInfosService} from "../services/student-infos.service";
import {Speciality} from "../models/speciality.enum";

@Component({
  selector: 'app-student-update',
  templateUrl: './student-update.component.html',
  styleUrls: ['./student-update.component.css']
})
export class StudentUpdateComponent implements OnInit {

  studentForm: FormGroup;
  @Input()
  student: StudentInfos;

  @Input() selectedSpeciality: Speciality;
  speciality = Speciality;

  constructor(private route: ActivatedRoute, private fb: FormBuilder , private studentService: StudentInfosService, private router : Router) {
    if(this.router.url.indexOf('/student/update/') !== -1) {
      this.studentForm = this.fb.group({
        email: ['', Validators.required],
        phone: ['', Validators.required]
      });
    }

    if(this.router.url.indexOf('/admin/update/') !== -1){
      this.studentForm = this.fb.group({
        email: ['', Validators.required],
        phone: ['', Validators.required],
        pseudo: ['', Validators.required],
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        speciality: ['', Validators.required],
        birthdate: ['', Validators.required]
      });
    }
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      if (params && params['id']) {
        this.studentService.get(params['id']).subscribe(fetchedStudent => this.student = fetchedStudent);
      }
    });
  }

  getSelectedValue(value : Speciality) {
    this.selectedSpeciality = value;
  }

  updateStudentByStudent() {
    if(this.student) {
      const formModel = this.studentForm.value
      const mstudent: StudentInfos = {
        id: this.student.id,
        pseudo: this.student.pseudo,
        lastname: this.student.lastname,
        firstname: this.student.firstname,
        birthdate: this.student.birthdate,
        speciality: this.student.speciality,
        email: formModel.email,
        phone: formModel.phone,
      };

      if (this.student && this.router.url == '/student/update/' + this.student.id) {
        this.studentService.update(mstudent).subscribe((a) => this.router.navigate(['/student/' + this.student.pseudo]));
      }
    }
  }
  updateStudentByAdmin() {
    if(this.student) {
      const formModel = this.studentForm.value
      let newSpeciality = this.student.speciality;
      if (this.selectedSpeciality) {
        newSpeciality = this.selectedSpeciality;
      }
      const mstudent: StudentInfos = {
        id: this.student.id,
        pseudo: formModel.pseudo,
        lastname: formModel.lastname,
        firstname: formModel.firstname,
        birthdate: formModel.birthdate,
        speciality: newSpeciality,
        email: formModel.email,
        phone: formModel.phone,
      };
      if (this.router.url == '/admin/update/' + this.student.id) {
        this.studentService.update(mstudent).subscribe((a) => this.router.navigate(['/admin/student/' + this.student.pseudo]));
      }
    }
  }

  getSpecialities() {
    return Object.values(Speciality);
  }

  isAdmin() {
    if(this.student) {
      return this.router.url === '/admin/update/' + this.student.id;
    }
  }

  isUtilisateur() {
    if(this.student) {
      return this.router.url === '/student/update/' + this.student.id;
    }
  }

}
