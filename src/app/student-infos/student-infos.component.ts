import { Component, OnInit, Input } from '@angular/core';
import {StudentInfos} from "../models/student-infos";
import {Router} from "@angular/router";

@Component({
  selector: 'app-student-infos',
  templateUrl: './student-infos.component.html',
  styleUrls: ['./student-infos.component.css']
})
export class StudentInfosComponent implements OnInit {

  @Input()
  studentInfos: StudentInfos;

  constructor(private router : Router) { }

  isAdmin() {
    return this.router.url === '/admin/student/'+this.studentInfos.pseudo;
  }

  isUtilisateur() {
    return this.router.url === '/student/'+this.studentInfos.pseudo;
  }

  ngOnInit() {
  }
}
