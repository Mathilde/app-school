export enum Speciality {
  ELECTRONIQUE_ENERGIES_SYSTEMES = "Électronique, Énergies, Systèmes",
  INFORMATIQUE = "Informatique",
  MATERIAUX = "Matériaux",
  PHOTONIQUE_SYSTEMES_OPTRONIQUES = "Photonique et Systèmes Optroniques"
}
