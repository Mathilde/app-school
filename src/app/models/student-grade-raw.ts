export interface StudentGradeRaw {
  student_id : number,
  note : number,
  coefficient : number,
  subject : string
}
