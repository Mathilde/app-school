import {Speciality} from "./speciality.enum";

export interface StudentInfos {
  id : number,
  pseudo : string,
  lastname : string,
  firstname : string,
  birthdate : Date,
  speciality : Speciality,
  email : string,
  phone : string
}
