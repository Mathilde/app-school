import {Speciality} from "./speciality.enum";

export interface RawStudentInfos {
  pseudo : string,
  lastname : string,
  firstname : string,
  birthdate : string,
  speciality : Speciality,
  email : string,
  phone : string
}
