export interface StudentGrade {
  id : number,
  student_id : number,
  note : number,
  coefficient : number,
  subject : string
}
