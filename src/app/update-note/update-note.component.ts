import { Component, OnInit, Input } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {StudentGrade} from "../models/student-grade";
import {ActivatedRoute, Router} from '@angular/router';
import {StudentGradeService} from "../services/student-grade.service";
import {StudentInfosService} from "../services/student-infos.service";
import {StudentInfos} from "../models/student-infos";

@Component({
  selector: 'app-update-note',
  templateUrl: './update-note.component.html',
  styleUrls: ['./update-note.component.css']
})
export class UpdateNoteComponent implements OnInit {

    gradeForm: FormGroup;
    @Input()
    grade: StudentGrade;
    id : number;
    studentInfos: StudentInfos;

    constructor(private route: ActivatedRoute, private fb: FormBuilder , private studentGradeService: StudentGradeService, private router : Router,
    private studentInfosService: StudentInfosService) {
      this.gradeForm = this.fb.group({
        note : ['', Validators.required ],
        coefficient : ['', Validators.required ],
        subject : ['', Validators.required ]
      });
    }

    ngOnInit() {
      this.route.params.subscribe( params => {
        if (params && params['id']) {
          this.id = params['id'];
        }
      });
      this.getgradeById(this.id);
    }

  public getgradeById(idValue: number) {
    this.studentGradeService.get(idValue).subscribe((grade) => {
      this.grade = grade;
      if(this.grade) {
        this.studentInfosService.get(this.grade.student_id).subscribe(student => this.studentInfos = student);
      }
    }
    );
  }

  updateGrade() {
     if(this.studentInfos && this.grade) {
       const formModel = this.gradeForm.value
       const mgrade: StudentGrade = {
         id: this.grade.id,
         student_id: this.grade.student_id,
         note: formModel.note,
         coefficient: formModel.coefficient,
         subject: formModel.subject,
       };
       this.studentGradeService.update(mgrade).subscribe((a) =>
         this.router.navigate(['/admin/student/' + this.studentInfos.pseudo])
       );
     }
   }
 }
