import {Component, OnInit, Input } from '@angular/core';
import {StudentInfos} from "../models/student-infos";
import {StudentInfosService} from "../services/student-infos.service";
import {ActivatedRoute, Router} from '@angular/router';
import {Speciality} from "../models/speciality.enum";


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  private _students : StudentInfos[];

  @Input() selectedSpeciality: Speciality;
  speciality = Speciality;


  constructor(private route: ActivatedRoute, private router : Router, private studentInfosService: StudentInfosService) {
    }

  getSpecialities() {
    return Object.values(Speciality);
  }

  students(): StudentInfos[]{
    return this._students;
  }

  ngOnInit() {
    this.studentInfosService.getAll().subscribe((students) => this._students = students);
  }

  onKey(searchValue : string) {
    this.studentInfosService.getAll().subscribe((students) => {
      this._students = students.filter(student => student.firstname.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1 ||
        student.lastname.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1 );
    });
  }

  onChange(event) {
    this.selectedSpeciality = event;
    if(this.selectedSpeciality) {
      this.studentInfosService.getAll().subscribe((students) => {
        this._students = students.filter(student => student.speciality == this.selectedSpeciality);
      });
    }
    else {
      this.studentInfosService.getAll().subscribe((students) => this._students = students);
    }
  }

  isNotNullAndNotEmpty() {
    if(this._students) {
      return this._students.length > 0;
    } else {
      return false;
    }
  }

}
