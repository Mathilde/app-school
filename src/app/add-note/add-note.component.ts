import {Component, Input, OnInit} from '@angular/core';
import {StudentInfosService} from "../services/student-infos.service";
import {StudentGradeService} from  "../services/student-grade.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StudentGradeRaw}from "../models/student-grade-raw";
import {StudentInfos} from "../models/student-infos";
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.css']
})

export class AddNoteComponent implements OnInit {

    noteForm: FormGroup;
    @Input()
    student: StudentInfos;

    constructor(private fb: FormBuilder, private studentgradeservice : StudentGradeService,
                private studentService : StudentInfosService, private router : Router , private route : ActivatedRoute) {
      this.noteForm = this.fb.group({
        note : ['', Validators.required ],
        coefficient : ['', Validators.required],
        subject : ['', Validators.required ]
      });
    }

    addNote(){
      if(this.student) {
        const formModel = this.noteForm.value;
        const newGrade: StudentGradeRaw = {
          student_id: this.student.id,
          note: formModel.note,
          coefficient: formModel.coefficient,
          subject: formModel.subject,
        }
        this.studentgradeservice.add(newGrade).subscribe((a) =>
          this.router.navigate(['/admin/student/' + this.student.pseudo])
        );
      }
    }

    ngOnInit() {
      this.route.params.subscribe( params => {
        if (params && params['id']) {
          this.studentService.get(params['id']).subscribe(fetchedStudent => this.student = fetchedStudent);
        }
      });
    }
  }
