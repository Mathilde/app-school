import { TestBed, inject } from '@angular/core/testing';

import { StudentInfosService } from './student-infos.service';

describe('StudentInfosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentInfosService]
    });
  });

  it('should be created', inject([StudentInfosService], (service: StudentInfosService) => {
    expect(service).toBeTruthy();
  }));
});
