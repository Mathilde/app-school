import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {StudentGradeRaw} from "../models/student-grade-raw";
import {StudentGrade} from "../models/student-grade";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';


@Injectable()
export class StudentGradeService {

  private url = "http://localhost:3000/grades";

  constructor(private http: HttpClient) { }

  public getByStudentId(student_id_in : number): Observable<StudentGrade[]> {
    return this.http.get<StudentGrade[]>(this.url+'?student_id='+student_id_in);
  }

  public get(id: number): Observable<StudentGrade> {
    return this.http.get<StudentGrade>(this.url+'/'+id);
  }

  public getAll(): Observable<StudentGrade[]> {
    return this.http.get<StudentGrade[]>(this.url);
  }

  public add(newnote: StudentGradeRaw): Observable<StudentGrade> {
    return this.http.post<StudentGrade>(this.url+'/', newnote);
  }

  public update(updategrade: StudentGrade): Observable<StudentGrade> {
    return this.http.put<StudentGrade>(this.url+'/' + updategrade.id, updategrade);
  }

  public delete(id: number): Observable<any> {
    return this.http.delete<any>(this.url+'/'+id);
  }

  getColumns(): string[] {
    return ["subject",  "note", "coefficient"];
  };
}
