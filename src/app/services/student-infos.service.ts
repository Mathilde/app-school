import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {StudentInfos} from "../models/student-infos";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import {RawStudentInfos} from "../models/student-raw-infos";

@Injectable()
export class StudentInfosService {

  private url = "http://localhost:3000/students";

    constructor(private http: HttpClient) {  }

  public get(id: number): Observable<StudentInfos> {
    return this.http.get<StudentInfos>(this.url+'/' + id);
  }

  public getAll(): Observable<StudentInfos[]> {
    return this.http.get<StudentInfos[]>(this.url);
  }

  public getByPseudo(pseudo_in: string): Observable<StudentInfos[]> {
    return this.http.get<StudentInfos[]>(this.url+'?pseudo=' + pseudo_in);
  }

  public add(newStudent: RawStudentInfos): Observable<StudentInfos> {
    return this.http.post<StudentInfos>(this.url+'/', newStudent);
  }

  public delete(id: number): Observable<any> {
    return this.http.delete<any>(this.url+'/'+id);
  }

  public update(updateStudent: StudentInfos): Observable<StudentInfos> {
    return this.http.put<StudentInfos>(this.url+'/' + updateStudent.id, updateStudent);
  }

}
