import { TestBed, inject } from '@angular/core/testing';

import { StudentGradeService } from './student-grade.service';

describe('StudentGradeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentGradeService]
    });
  });

  it('should be created', inject([StudentGradeService], (service: StudentGradeService) => {
    expect(service).toBeTruthy();
  }));
});
