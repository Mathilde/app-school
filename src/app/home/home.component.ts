import {Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  enterForm: FormGroup;
  pseudo: string;

  constructor(private fb: FormBuilder, private router : Router, private route: ActivatedRoute) {
    this.enterForm = this.fb.group({
      pseudo: ['', Validators.required ],
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.pseudo = params['pseudo'];
    });
  }

  enter() {
    const formModel = this.enterForm.value;
    this.pseudo = formModel.pseudo;
    if(this.pseudo == 'admin') {
      this.router.navigate(['/admin/students/']);
    } else {
      this.router.navigate(['/student/' + this.pseudo]);
    }
  }
}
