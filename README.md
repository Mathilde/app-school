# AppSchool - Imen, Mohamed et Mathilde - Application de gestion des étudiants

Avant de lancer le projet :
#npm install --save replace-in-file

Commande pour lancer la base de données : 
#json-server --watch db.json


Fonctionnalités :
Page d'accueil : pour se connecter au site il faut saisir son pseudo ou "admin" pour passer en mode administrateur

Mode utilisateur : consultation de ses informations et de ses notes (+ la moyenne) et possibilités de modifier certaines de ses infos

Mode administrateur : 
- affichage de la liste de tous les étudiants (possibilités de filtrer en fonction de la spécialité ou du nom / prénom de l'étudiant : insensible à la casse)
- Ajout d'un étudiant
- Consulter la fiche des étudiants (informations et notes)
- Modifier toutes les informations de l'étudiant (via sa fiche)
- Ajouter et modifier les notes de l'étudiant (via sa fiche)


Application : 
Consultation des informations sur les étudiants (admin et utilisateur) en mode hors ligne ou en ligne.

Site web :
Consultation et modification des informations (admin et utilisateur) en mode en ligne.
